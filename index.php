<!doctype html>
  <head>
    <meta charset="utf-8">
    <title>Atelier Bek - portfolio</title>
    <link rel="stylesheet" type="text/css" href="style/style.css" />
	  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.lazyload.min.js"></script>
  </head>
  <body>

  <div id="bg" class="info">
    <?php
      $infos = file_get_contents('infos.txt');
      echo $infos;
    ?>
  </div>

  <?php

      // On met les direction de data dans l'array dirProjet
      $dirProjet = array();
      $MyDirectory = opendir('data') or die('Erreur');
      while($Entry = readdir($MyDirectory)) {
          if(is_dir($Directory.'/'.$Entry)&& $Entry != '.' && $Entry != '..') {
          }
            elseif ($Entry !='.' && $Entry != '..') {
	      array_push($dirProjet,$Entry);
          }
      }
      closedir($MyDirectory);

      sort($dirProjet);

      //On compte le nombre de repertoire dans le dossier data
      $count=count($dirProjet);

      // boucle de tout les projets
      for ($i = 0; $i < $count; $i++){

	?>
	<div class="projet projet<?php echo $i; ?>" >
	<?php

        $dir = 'data/'.$dirProjet[$i];
       	$load_text = $dir.'/00texte.txt';
	$info_read = file_get_contents($load_text);
	?>
	    <div class="legende">
        <?php echo $info_read; ?>
        <h3><?php echo $i+1; ?>/<?php echo $count;?></h3>
      </div>
	    <div class="slider">
      <?php
	if (is_dir($dir)) {
	  if ($dh = opendir($dir)) {

		$j=0;

		$images=array();

		while (($file = readdir($dh)) !== false) {
		    $images[]=$file;
		}
		sort($images);

    $first = true;
      foreach($images as $file){

            if( $file != '.' && $file != '..' && preg_match('#\.(jpe?g|gif|png|svg)$#i', $file)) {

            if($first){
              ?>
                <img class="<?php echo $j; ?>" src="<?php echo $dir.'/'.$file; ?>" />
              <?php
              $first = false;
              $j++;
            } else {

            ?>
            <img class="<?php echo $j; ?>" src="" data-original="<?php echo $dir.'/'.$file; ?>" />
            <?php

          $j++;
      }
        }
              }
              closedir($dh);
          }
      }

?>	  </div>
	</div>
<?php
      }
      ?>
	  <script type="text/javascript" src="js/pages.js"></script>
	  <script type="text/javascript" src="js/slider.js"></script>
	  <script type="text/javascript" src="js/imagesizer.js"></script>
	  <script type="text/javascript" src="js/gabarit.js"></script>
	  <script type="text/javascript" src="js/sizefont.js"></script>
	  <script type="text/javascript" src="js/main.js"></script>
  </body>
</html>
