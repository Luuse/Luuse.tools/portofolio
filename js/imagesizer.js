function imagesizer(){

  var projet  = $('.projet');
  var images  = projet.children('.slider').children('img');

  // Get window width an height
  var windowWidth = $(window).width();
  var windowHeight = $(window).height();

  images.each(function(){

    var imageWidth = $(this).width();
    var imageHeight = $(this).height();

    // Scale image width to window width
    if (windowWidth > windowHeight){

      $(this).height(windowHeight-180)
      $(this).width('auto');
      var imageWidth = $(this).width();
      $(this).css({'margin-left': (windowWidth-imageWidth)/2})

    // Scale image height to window height when window height is greater than image width
    } else if (windowWidth < windowHeight){

      $(this).width(windowWidth-100)
      $(this).height('auto');
      var imageHeight = $(this).height();
      $(this).css({'margin-top': (windowHeight-imageHeight)/2});
      $(this).css({'margin-bottom': (windowHeight-imageHeight)/2});
      $(this).css({'margin-left': '50px'});

    }

  })

}
