function gabarit(){

  var projet = $('.projet');
  var legende = projet.children('.legende');
  var windowHeight = $(window).height();
  var documentHeight = $(document).height();

  legende.children('h1').hide();
  legende.children('h2').hide();
  legende.children('h3').hide();
  legende.children('p').hide();

    var projetsY = [];

    for(var i=0; i<projet.length; i++){
      projetsY.push($('.projet'+i).offset().top);
    }
  $(window).scroll(function(){
    var scrollY = $(this).scrollTop();

    for (var i=0; i<projet.length; i++){
      if (projetsY[i] < scrollY+(windowHeight/2)){
        $('.projet').children('.legende').children('h1').hide();
        $('.projet').children('.legende').children('h2').hide();
        $('.projet').children('.legende').children('h3').hide();
        $('.projet').children('.legende').children('p').hide();
        $('.projet'+i).children('.legende').children('h1').show();
        $('.projet'+i).children('.legende').children('h2').show();
        $('.projet'+i).children('.legende').children('h3').show();
        $('.projet'+i).children('.legende').children('p').show();
      }

    }
    console.log(scrollY + ' -- ' + (documentHeight-windowHeight));

    if(scrollY >= (documentHeight-windowHeight*1.5)){
        $('.projet').children('.legende').children('h1').hide();
        $('.projet').children('.legende').children('h2').hide();
        $('.projet').children('.legende').children('h3').hide();
        $('.projet').children('.legende').children('p').hide();
    }

  })

}
