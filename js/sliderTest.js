function slider(){

  var projet  = $('.projet');

  projet.each(function(){

    if ($(this).children(".legende").children('iframe').length > 0){
      var video=$(this).children().children('iframe');
      $(this).children(".slider").append('<div class="video"></div>');
      $(this).children(".slider").children('.video').prepend('<div class="btn"></div>');
      var btn_height = $(this).children('.video').children('iframe').height();
      $(this).children(".slider").children('.video').children('.btn').height('50%');
      $(this).children(".slider").children('.video').append(video);
    };

    var slider  = $(this).children('.slider');
    var total = slider.children().length;
    var nb = 0;

    if ($(this).children(".slider").children('.video').length > 0){
      $(this).children(".slider").children('.video').addClass('11');
    };

    slider.children().hide();
    slider.children('.0').show();

    slider.children().click(function(){

    if(nb == total-1){
      nb = 0;
    }else {
      nb=nb+1;
    }

      var imagesCount = slider.children('img').length;
      
      for(var i=0; i < 10; i++){
	var src = slider.children('.'+i).attr('data-original');
	slider.children('.'+i).attr('src', src);
      }
        

      $(this).hide();
      slider.children('.'+nb).show();

      var windowWidth = $(window).width();
      var imageWidth = slider.children('.'+nb).width();
      var marginleft = (windowWidth-imageWidth)/2;

      slider.children('.'+nb).css({'margin-left': marginleft});

    })

  })

}
