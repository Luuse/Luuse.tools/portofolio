function slider(){
  var windHeight = $(window).height();
  $('img').height(windHeight/1.2); 

  var projet  = $('.projet');

  projet.each(function(){

    var slider  = $(this).children('.slider');
    var total = slider.children().length;
    var nb = 0;
    var bloc = $(this);
    
    if ($(this).children(".legende").children('iframe').length > 0){
      var video = $(bloc).children().children('iframe');
      var nm_video = 0;
      video.each(function () {
        $(slider).append('<div class="video vd'+ nm_video +'"></div>');
        $(slider).children('.vd'+ nm_video).prepend('<div class="btn"></div>');
        $(slider).children('.vd'+ nm_video).append($(this));
        $(slider).children('.vd'+ nm_video).addClass(''+total);
        nm_video++;
        total++;
      });
    };

    var total2 = slider.children().length;



    slider.children().hide();
    slider.children('.0').show();

    slider.children().click(function(){

      if(nb == total2-1){
        nb = 0;
      } else {
        nb=nb+1;
      }

      var src = $(this).next('img').attr('data-original');
      $(this).next('img').attr('src', src);

      $(this).hide();
      slider.children('.'+nb).show();

      //var windowWidth = $(window).width();
      //var imageWidth = slider.children('.'+nb).width();
      //var marginleft = (windowWidth-imageWidth)/2;

      //slider.children('.'+nb).css({'margin-left': marginleft});
      //slider.children('.video').css({'margin-left': 0});

    })

  })

}
