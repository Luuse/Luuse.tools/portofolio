function sizefont(block, divis, minSize) {
  var sizeWind = $(this).width();
  var sizeFont = sizeWind / divis;   
  if (sizeFont < 14){ 
      $('.'+block).css('font-size', minSize+'px');
    }else{
      $('.'+block).css('font-size', sizeFont+'px');
    };
  
  $(window).resize(function(){
    sizeWind = $(this).width();
    sizeFont = sizeWind / divis;
    if (sizeFont < minSize){ 
      $('.'+block).css('font-size', minSize+'px');
    }else{
      $('.'+block).css('font-size', sizeFont+'px');
    };
  });   
}
