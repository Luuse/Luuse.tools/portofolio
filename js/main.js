$(document).ready(function(){

  $('.projet').hide();

  //sizefont argument -> class, division, taille minimum
  sizefont('pages', 80, 14);
  sizefont('info', 55, 20);

  var bgH = $('#bg').height();
  var windowH = $(window).height();
  var projets = $('.projet');
  var totalProjets = projets.length;
  var lastProjet = totalProjets-1;

  // Texte background
  $('.projet0').css({'margin-top': bgH-60}); // margin du premier projet en dessous du texte en background
  $('.projet'+lastProjet).css({'margin-bottom': windowH}); // margin du dernier projet hauteur fenetre

  projets.children('.slider').children('img').mouseleave(function(){
    $('#bg').css({'z-index': '1000'});
    projets.children('.legende').children('p').css({'z-index': '1000'});
  }).mouseenter(function(){
    $('#bg').css({'z-index': '-1000'});
    projets.children('.legende').children('p').css({'z-index': '-1000'});
  })

  // Textes projets
  $('br').after('<span class="indent"></span>'); // indentation des paragraphes

})

$(window).load(function(){

  $('.projet').show();

  slider();
  //imagesizer();
  gabarit();
  

})

$(window).resize(function(){
  //imagesizer();

})
